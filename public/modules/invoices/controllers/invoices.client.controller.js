'use strict';
/*globals _, moment*/
// Invoices controller
angular.module('invoices').controller('InvoicesController', ['$scope', '$stateParams', '$location', 'Authentication',
	'Invoices', 'Projects', 'coreConfig','$http','$window',
	function($scope, $stateParams, $location, Authentication, Invoices, Projects, coreConfig, $http, $window) {
		$scope.authentication = Authentication;
		$scope.invoice = {invoiceDate:coreConfig.date};


		var getInvoiceNumber = function(invoices){
			var tmp;
			if(invoices && invoices.length && invoices[0].name){
				tmp = invoices[0].name.split('-')[1];
				tmp = tmp/1 + 1;
				if(tmp<10){
					tmp = '0'+tmp;
				}
				if(tmp<100){
					tmp = '0'+tmp;
				}
			} else {
				tmp = '001';
			}
			if($scope.project.invoicePrefix.slice(-1)==='-'){
				$scope.project.invoicePrefix =$scope.project.invoicePrefix.slice(0,-1);
			}
			return  $scope.project.invoicePrefix+'-'+tmp;
		};

		var setInvoiceNumber = function(project){

			if(project.invoices){
				$scope.invoice.name = getInvoiceNumber(project.invoices);
			} else {
				$http.get('/invoices/byProject/'+project._id).success(function(data){
					project.invoices = data;
					$scope.invoice.name = getInvoiceNumber(project.invoices);
				}).error(function(){
					alert('Not able to generate invoice number');
				});
			}
		};

		var projectSelected = function(projectId){
			var project = $scope.project = _.find($scope.projects,{_id:projectId});
			var invoice = $scope.invoice;
			//Set Invoice number based on recent invoice & prefix
			setInvoiceNumber(project);
			invoice.billTo = project.vendor.name;
			invoice.client = project.client;
			invoice.dueDate = moment(invoice.invoiceDate).add(project.netDue,'days').format('YYYY-MM-DD');
			invoice.sentTo = ''; //project.vendor.emails;
			_.forEach( project.vendor.emails,function(obj,key){
				invoice.sentTo += obj.email + ', ';
			});
			invoice.consultantName = project.user.firstName + ' ' + project.user.lastName;
			invoice.sentTo = invoice.sentTo.slice(0,-2);
			invoice.location = (project.location?project.location.city+', ':'') + project.location?project.location.state:'';
			invoice.invoiceAddress = project.vendor.address.city + ', ' + project.vendor.address.state;
		};

		$scope.dateChange = function(){
			$scope.hasHours = false;
		};

		$scope.fetchHours = function(){
			var invoice = $scope.invoice;
			if(!invoice.fromDate || !invoice.toDate){
				return alert('Please enter both the dates');
			}
			var postData = {project:$scope.invoice.project,startDate:invoice.fromDate,endDate:invoice.toDate};
			$http.post('/reports/timesheets', postData).
				success(function(tsData, status, headers, config) {

					//TODO: need to refactor divide at logic to accommodate all possible dates instead of just two
					/*
					var divideAt;
					if($scope.project.rate.length > 1){
					 $scope.project.rate=_.sortBy($scope.project.rate, 'effectiveDate').reverse();
						if(postData.startDate < $scope.project.rate[0].effectiveDate && postData.dateDate > $scope.project.rate[0].effectiveDate){
							divideAt = moment($scope.project.rate[0].effectiveDate).format('YYYY-MM-DD');
						}
					};*/
					var start = moment(postData.startDate).format('YYYY-MM-DD');
					var end = moment(postData.endDate).format('YYYY-MM-DD');
					var total = 0;
					var statuses = {};

					_.forEach(tsData,function(obj,key){
						_.remove(tsData[key].daily, function(day){
							if(day.date < start || day.date > end){ return true;}
						});
						tsData[key].hours = _.reduce(tsData[key].daily, function(result, day) {
							return result+ (day.hours?day.hours:0);
						}, 0);
						total += tsData[key].hours;
						if(statuses[tsData[key].status]){
							statuses[tsData[key].status]+=tsData[key].hours;
						} else {
							statuses[tsData[key].status]=tsData[key].hours;
						}
					});

					/*
					if(divideAt){
						_.forEach(tsData,function(obj,key){
							_.forEach(tsData[key].daily, function(obj,key){
								if(obj.day<divideAt){

								}
							});
						});
					}*/

					$scope.project.rate=_.sortBy($scope.project.rate, 'effectiveDate').reverse();
					var rate = 0;
					if($scope.project.rate && $scope.project.rate.length){
						rate = $scope.project.rate[0].rate;
					}

					$scope.invoice.items = [{
						description: 'Service from ' + moment(postData.startDate).format('MMM DD, YYYY') + ' to ' +  moment(postData.endDate).format('MMM DD, YYYY'),
						rate: rate,
						hours: total
					}];

					$scope.tsStatuses = statuses;
					$scope.tsTotal = total;
					$scope.hasHours = true;
				}).
				error(function(data, status, headers, config) {
					alert('Error while getting report');
				});
		};

		$scope.previewInvoice = function(action){

			$scope.invoice.total = $scope.getTotal();
			$http.post('/invoices/preview', {invoice:$scope.invoice, company:$scope.company, action:action}).success(function(data){
				var wnd;
				if(data && data.success){
					if(data.invoiceURL){
						//Must use window instead of $window service to make sure that window keeps ope even on location change
						wnd = window.open(data.invoiceURL, '', '_blank');
					}
					return $location.path('invoices');
				}
				if(!action){
					wnd = $window.open('about:blank', '', '_blank');
					wnd.document.write(data + '<br><br><button onclick="window.close()">Close</button>');
				}
			});
		};

		// Create new Invoice
		$scope.create = function() {
			// Create new Invoice object
			var invoice = new Invoices ({
				name: this.name
			});

			// Redirect after save
			invoice.$save(function(response) {
				$location.path('invoices/' + response._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Invoice
		$scope.remove = function( invoice ) {
			if ( invoice ) { invoice.$remove();

				for (var i in $scope.invoices ) {
					if ($scope.invoices [i] === invoice ) {
						$scope.invoices.splice(i, 1);
					}
				}
			} else {
				$scope.invoice.$remove(function() {
					$location.path('invoices');
				});
			}
		};

		// Update existing Invoice
		$scope.update = function() {
			var invoice = $scope.invoice;
			invoice.$update(function() {
				$location.path('invoices/' + invoice._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Invoices
		$scope.find = function() {
			$scope.invoices = Invoices.query();
		};

		// Find existing Invoice
		$scope.findOne = function() {
			$scope.invoice = Invoices.get({ 
				invoiceId: $stateParams.invoiceId
			},function(data){
				$scope.project = _.find($scope.projects,{_id:data.project});
			});
		};

		$scope.createLanding = function(){

			$scope.action = 'New';

			$http.get('/getCompanyDetails').success(function(data){
				$scope.company = data;
			});

			$scope.projects = Projects.getList(function(){
				if($stateParams.projectId){
					$scope.invoice.project = $stateParams.projectId;
					projectSelected($stateParams.projectId);
				}
				if($stateParams.invoiceId){
					$scope.action = 'New';
					$scope.findOne();
				}
			});
		};

		//Project changes
		$scope.selectProject = function(){
			projectSelected($scope.invoice.project);
		};

		$scope.markInvoice = function(status){
			$http.post('invoices/'+$scope.invoice._id+'/changeStatus',{status:status}).success(function(data){
				$scope.statusBtnEnabled = false;
				$scope.invoice = data;
			});
		};

		$scope.getTotal = function(){
			var items = $scope.invoice.items;
			if(!items){
				return 0;
			}
			var total = 0;
			for(var i=0;i<items.length;i++){
				total += (items[i].rate*items[i].hours);
			}
			return total;
		};
	}
]);