'use strict';

// Configuring the Articles module
angular.module('expenses').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Expenses', 'expenses', 'dropdown', '/expenses(/create)?',false,['admin']);
		Menus.addSubMenuItem('topbar', 'expenses', 'List Expenses', 'expenses');
		Menus.addSubMenuItem('topbar', 'expenses', 'Add New Expense', 'expenses/create');
	}
]);